FROM alpine
RUN apk update && apk add spamassassin ca-certificates perl-utils clamav-libunrar make clamav && \
    PERL_MM_USE_DEFAULT=1 cpan -T File:Scan:ClamAV && \
    apk del make clamav && \
    sa-update
CMD /bin/sh -c "sa-update;spamd"

